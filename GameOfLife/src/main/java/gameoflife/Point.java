/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gameoflife;

/**
 *
 * @author usuari
 */
public class Point {

    private int row;
    private int col;
    private boolean alive;

    public Point(int row, int col) {
        this.row = row;
        this.col = col;
    }

    public Point(int row, int col, boolean alive) {
        this(row, col);
        this.alive = alive;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

}
