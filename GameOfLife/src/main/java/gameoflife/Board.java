/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gameoflife;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 *
 * @author usuari
 */
public class Board extends javax.swing.JPanel {

    class myMouseAdapter extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent e) {
            int row = (int) e.getPoint().getX() / squareWidht();
            int col = (int) e.getPoint().getY() / squareHeight();
            Point p = new Point(row, col);
            pointClicked(p);
        }
    }

    private static final int NUM_ROWS = 100, NUM_COLS = 100;
    private Point[][] currentMap, nextMap;

    public Board() {
        super();
        myInit();
    }

    private void myInit() {
        currentMap = new Point[NUM_ROWS][NUM_COLS];
        for (int row = 0; row < NUM_ROWS; row++) {
            for (int col = 0; col < NUM_COLS; col++) {
                currentMap[row][col] = new Point(row, col, false);
                nextMap[row][col] = new Point(row, col, false);
            }
        }
    }

    private void pointClicked(Point p) {
        currentMap[p.getRow()][p.getCol()].setAlive(!currentMap[p.getRow()][p.getCol()].isAlive());
    }

    public int squareWidht() {
        return getWidth() / NUM_COLS;
    }

    public int squareHeight() {
        return getHeight() / NUM_ROWS;
    }
}
